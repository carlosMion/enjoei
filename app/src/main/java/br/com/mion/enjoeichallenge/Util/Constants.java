package br.com.mion.enjoeichallenge.Util;

/**
 * Created by Carlos Mion on 27/06/2018.
 * E-mails :
 * mioncarlos23@gmail.com
 * carlosmion23@hotmail.com
 */
public class Constants {

    public static final String BASE_IP = "https://private-anon-377b2ab535-enjoeitest.apiary-mock.com/";
    public static final String URL_GET_PRODUCTS = "products/home";
    public static final String URL_PHOTO = "http://res.cloudinary.com/demo/image/upload/c_fill,g_auto,w_150,h_200/";
    public static final Integer COLOR_ACCENT = -560524;

}
