package br.com.mion.enjoeichallenge.SalesList.View;

/**
 * Created by Carlos Mion on 26/06/2018.
 * E-mails :
 * mioncarlos23@gmail.com
 * carlosmion23@hotmail.com
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.mion.enjoeichallenge.R;
import br.com.mion.enjoeichallenge.Retrofit.ListProducts.POJOs.Photo;
import br.com.mion.enjoeichallenge.Retrofit.ListProducts.POJOs.Product;
import br.com.mion.enjoeichallenge.Util.BackgroundColor;
import br.com.mion.enjoeichallenge.Util.Constants;

import static br.com.mion.enjoeichallenge.Util.Constants.COLOR_ACCENT;
import static br.com.mion.enjoeichallenge.Util.TextOperations.getTText;

/**
 * Created by Ravi Tamada on 18/05/16.
 */
public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.MyViewHolder> {

    private Context context;
    private MainActivity mainActivity;
    private List<Product> itemsList;

    public ItemsAdapter(Context mContext, List<Product> albumList, MainActivity mainActivity) {
        this.context = mContext;
        this.itemsList = albumList;
        this.mainActivity = mainActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_card_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Product product = itemsList.get(position);
        holder.title.setText(product.getTitle());
        holder.likes_count.setText(String.valueOf(product.getLikesCount()));
        holder.id.setText(String.valueOf(product.getId()));

//         loading album cover using Glide library
        for (Photo photo : product.getPhotos()) {
            if (!photo.getPublicId().equals("sample")) {
                Glide.with(context).asBitmap().load(Constants.URL_PHOTO + photo.getPublicId()).into(holder.thumbnail);
                break;
            }
        }

        Glide.with(context).asBitmap().load(Constants.URL_PHOTO + product.getUser().getAvatar().getPublicId()).into(holder.avatar);

        String price = "R$ " + String.valueOf(product.getPrice());

        holder.price.setText(price);

        if (product.getSize() != null)
            setSize(holder.size, product.getSize());


        holder.ll_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                likeItem(holder);
            }
        });

        holder.ll_mainInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemDetails(getTText(holder.id), getTText(holder.price));
            }
        });

    }

    private void setSize(TextView tv_size, String size) {

        String sizeText = "- tam " + size;

        tv_size.setText(sizeText);

    }

    private void itemDetails(String id, String price) {

        mainActivity.getItemDetails(id, getOnlyPriceValue(price));

    }

    private String getOnlyPriceValue(String price) {
        return price.split(" ")[1];
    }

    private void likeItem(MyViewHolder holder) {

        if (BackgroundColor.getBackgroundColor(holder.ll_like) != COLOR_ACCENT) {
            holder.ll_like.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
            int likeCounts = Integer.parseInt(getTText(holder.likes_count));
            holder.likes_count.setText(String.valueOf(++likeCounts));
        } else {
            holder.ll_like.setBackgroundColor(context.getResources().getColor(R.color.viewBackground));
            int likeCounts = Integer.parseInt(getTText(holder.likes_count));
            holder.likes_count.setText(String.valueOf(--likeCounts));
        }
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView thumbnail, btnLike, avatar;
        TextView title, likes_count, price, size, id;
        LinearLayout ll_like, ll_mainInfo;

        MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            likes_count = view.findViewById(R.id.likes_count);
            price = view.findViewById(R.id.tv_price);
            thumbnail = view.findViewById(R.id.thumbnail);
            btnLike = view.findViewById(R.id.btn_like);
            avatar = view.findViewById(R.id.avatar);
            ll_like = view.findViewById(R.id.ll_like);
            ll_mainInfo = view.findViewById(R.id.MainInfo);
            id = view.findViewById(R.id.id);
            size = view.findViewById(R.id.tv_size);
        }
    }
}
