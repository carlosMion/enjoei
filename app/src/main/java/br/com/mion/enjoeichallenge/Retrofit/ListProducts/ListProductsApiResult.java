package br.com.mion.enjoeichallenge.Retrofit.ListProducts;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import br.com.mion.enjoeichallenge.Retrofit.ListProducts.POJOs.Pagination;
import br.com.mion.enjoeichallenge.Retrofit.ListProducts.POJOs.Product;

/**
 * Created by Carlos Mion on 27/06/2018.
 * E-mails :
 * mioncarlos23@gmail.com
 * carlosmion23@hotmail.com
 */
public class ListProductsApiResult {
    @SerializedName("products")
    @Expose
    private List<Product> products = null;
    @SerializedName("pagination")
    @Expose
    private Pagination pagination;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }
}