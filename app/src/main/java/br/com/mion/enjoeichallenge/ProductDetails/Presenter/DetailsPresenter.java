package br.com.mion.enjoeichallenge.ProductDetails.Presenter;

import br.com.mion.enjoeichallenge.ProductDetails.MVPDetails;
import br.com.mion.enjoeichallenge.ProductDetails.Model.DetailsModel;

/**
 * Created by Carlos Mion on 01/07/2018.
 * E-mails :
 * mioncarlos23@gmail.com
 * carlosmion23@hotmail.com
 */
public class DetailsPresenter implements MVPDetails.Presenter {
    /**
     * Este Presenter foi deixado aqui de propósito, para demonstrar que caso seja necessário, basta utilizá-lo
     */


    private MVPDetails.View view;
    private MVPDetails.Model model;

    public DetailsPresenter() {

        this.model = new DetailsModel();
        model.setPresenter(this);
    }

    @Override
    public void setView(MVPDetails.View view) {
        this.view = view;
    }
}
