package br.com.mion.enjoeichallenge.ProductDetails;

/**
 * Created by Carlos Mion on 01/07/2018.
 * E-mails :
 * mioncarlos23@gmail.com
 * carlosmion23@hotmail.com
 */
public interface MVPDetails {


    interface View {


    }

    interface Presenter {


        void setView(View view);
    }

    interface Model {


        void setPresenter(MVPDetails.Presenter presenter);
    }

}