package br.com.mion.enjoeichallenge.SalesList.View;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.github.nuptboyzhb.lib.SuperSwipeRefreshLayout;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.ArrayList;
import java.util.List;

import br.com.mion.enjoeichallenge.ProductDetails.View.DetailsFragment;
import br.com.mion.enjoeichallenge.R;
import br.com.mion.enjoeichallenge.Retrofit.ListProducts.POJOs.Product;
import br.com.mion.enjoeichallenge.SalesList.MVPSalesList;
import br.com.mion.enjoeichallenge.SalesList.Presenter.SalesListPresenter;
import br.com.mion.enjoeichallenge.Util.UINavigation;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

import static br.com.mion.enjoeichallenge.Util.Conversions.dpToPx;
import static br.com.mion.enjoeichallenge.Util.TextOperations.getTText;

public class MainActivity extends AppCompatActivity implements MVPSalesList.View {

    @BindView(R.id.content_main)
    RelativeLayout contentMain;

    @BindView(R.id.content_main_error)
    LinearLayout contentMainError;

    @BindView(R.id.main_layout)
    CoordinatorLayout main_layout;

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;

    @BindView(R.id.appbar)
    AppBarLayout appbar;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.loading)
    SpinKitView loadingView;

    @BindView(R.id.fragment_placeholder)
    FrameLayout fragment_placeholder;

    @BindView(R.id.swipe_refresh)
    SuperSwipeRefreshLayout swipeRefreshLayout;


    private ItemsAdapter adapter;
    private List<Product> itemsList;
    private MVPSalesList.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        initPresenter();

        initCollapsingToolbar();

        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(this, 10), false));

        itemsList = new ArrayList<>();

        setUpTextFont();

        getItemList();

        setUpPullToRefresh();
    }

    private void setUpPullToRefresh() {


        swipeRefreshLayout
                .setOnPullRefreshListener(new SuperSwipeRefreshLayout.OnPullRefreshListener() {

                    @Override
                    public void onRefresh() {
                        getItemList();
                    }

                    @Override
                    public void onPullDistance(int distance) {

                    }

                    @Override
                    public void onPullEnable(boolean enable) {

                    }
                });

    }

    private void setUpTextFont() {

        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/ProximaNova-Regular.otf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());

    }

    private void setUpScreen() {

        swipeRefreshLayout.setRefreshing(false);

        adapter = new ItemsAdapter(this, itemsList, this);

        recyclerView = findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        try {
            Glide.with(this).load(R.drawable.banner).into((ImageView) findViewById(R.id.backdrop));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initPresenter() {

        presenter = new SalesListPresenter();
        presenter.setView(this);

    }

    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */
    private void initCollapsingToolbar() {
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(R.string.app_name));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    private void getItemList() {

        presenter.getItemsList();

    }


    @Override
    public void showErrorScreen() {
        hideWholeMainScreen();
        contentMainError.setVisibility(View.VISIBLE);

        loadingView.setVisibility(View.GONE);

        Button btnTryAgain = findViewById(R.id.btn_try_again);

        btnTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getItemList();
            }
        });


    }

    @Override
    public void showMainScreen() {
        contentMain.setVisibility(View.VISIBLE);
        appbar.setVisibility(View.VISIBLE);
        fragment_placeholder.setVisibility(View.GONE);
        hideErrorScreen();
        loadingView.setVisibility(View.GONE);

    }

    private void hideErrorScreen() {
        contentMainError.setVisibility(View.GONE);
    }

    @Override
    public void showLoadingScreen() {
        hideWholeMainScreen();
        hideErrorScreen();
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void updateProducts() {

        setUpScreen();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void setProductsListOnMain(List<Product> products) {
        this.itemsList = products;
    }

    private void hideWholeMainScreen() {
        contentMain.setVisibility(View.GONE);
        appbar.setVisibility(View.GONE);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    public void getItemDetails(String id, String price) {
        DetailsFragment detailsFragment = new DetailsFragment();
        for (Product product : itemsList) {
            if (getTText(product.getId()).equals(id) && getTText(product.getPrice()).equals(price)) {
                hideWholeMainScreen();
                detailsFragment.setProduct(product);
                UINavigation.callFragment(detailsFragment, "DetailsFragment", this);
                break;
            }
        }

    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            showMainScreen();
        }
        super.onBackPressed();


    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }
}
