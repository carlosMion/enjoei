package br.com.mion.enjoeichallenge.Retrofit.ListProducts.POJOs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Carlos Mion on 27/06/2018.
 * E-mails :
 * mioncarlos23@gmail.com
 * carlosmion23@hotmail.com
 */
public class Product {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("discount_percentage")
    @Expose
    private Integer discountPercentage;
    @SerializedName("photos")
    @Expose
    private List<Photo> photos = null;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("price")
    @Expose
    private Float price;
    @SerializedName("original_price")
    @Expose
    private Float originalPrice;
    @SerializedName("size")
    @Expose
    private String size;
    @SerializedName("likes_count")
    @Expose
    private Integer likesCount;
    @SerializedName("maximum_installment")
    @Expose
    private Integer maximumInstallment;
    @SerializedName("published_comments_count")
    @Expose
    private Integer publishedCommentsCount;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("user")
    @Expose
    private User user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(Integer discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(Float originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Integer getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(Integer likesCount) {
        this.likesCount = likesCount;
    }

    public Integer getMaximumInstallment() {
        return maximumInstallment;
    }

    public void setMaximumInstallment(Integer maximumInstallment) {
        this.maximumInstallment = maximumInstallment;
    }

    public Integer getPublishedCommentsCount() {
        return publishedCommentsCount;
    }

    public void setPublishedCommentsCount(Integer publishedCommentsCount) {
        this.publishedCommentsCount = publishedCommentsCount;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


}