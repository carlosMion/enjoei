package br.com.mion.enjoeichallenge.SalesList.Model;

import br.com.mion.enjoeichallenge.Retrofit.ListProducts.ListProductsAPI;
import br.com.mion.enjoeichallenge.Retrofit.ListProducts.ListProductsApiResult;
import br.com.mion.enjoeichallenge.Retrofit.RetrofitInstance;
import br.com.mion.enjoeichallenge.SalesList.MVPSalesList;
import br.com.mion.enjoeichallenge.Util.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Carlos Mion on 27/06/2018.
 * E-mails :
 * mioncarlos23@gmail.com
 * carlosmion23@hotmail.com
 */
public class SalesListModel implements MVPSalesList.Model {

    private MVPSalesList.Presenter presenter;

    @Override
    public void setPresenter(MVPSalesList.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void getItemsList() {

        ListProductsAPI service = RetrofitInstance.getRetrofitInstance().create(ListProductsAPI.class);
        Call<ListProductsApiResult> call = service.getProductsList(Constants.URL_GET_PRODUCTS);

        call.enqueue(new Callback<ListProductsApiResult>() {
            @Override
            public void onResponse(Call<ListProductsApiResult> call, Response<ListProductsApiResult> response) {
                if (response.body() != null && response.body().getProducts() != null) {
                    presenter.parseProducts(response.body().getProducts());
                } else
                    presenter.showErrorScreen();
            }

            @Override
            public void onFailure(Call<ListProductsApiResult> call, Throwable t) {
                presenter.showErrorScreen();
            }
        });

    }


}