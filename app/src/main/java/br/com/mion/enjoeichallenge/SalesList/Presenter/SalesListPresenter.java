package br.com.mion.enjoeichallenge.SalesList.Presenter;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import br.com.mion.enjoeichallenge.Retrofit.ListProducts.POJOs.Product;
import br.com.mion.enjoeichallenge.SalesList.MVPSalesList;
import br.com.mion.enjoeichallenge.SalesList.Model.SalesListModel;

/**
 * Created by Carlos Mion on 28/06/2018.
 * E-mails :
 * mioncarlos23@gmail.com
 * carlosmion23@hotmail.com
 */
public class SalesListPresenter implements MVPSalesList.Presenter {

    private MVPSalesList.View view;
    private MVPSalesList.Model model;
    private ArrayList<ArrayList<Bitmap>> pictures;
    private int lastProductId = 0;
    private String lastPublicId = "";
    private List<Product> products;
    private boolean doneGettingPics = false;

    public SalesListPresenter() {

        pictures = new ArrayList<>();
        this.model = new SalesListModel();
        model.setPresenter(this);

    }

    @Override
    public void setView(MVPSalesList.View view) {
        this.view = view;
    }

    @Override
    public void getItemsList() {
        view.showLoadingScreen();
        model.getItemsList();
    }

    @Override
    public void showErrorScreen() {
        view.showErrorScreen();
    }

    @Override
    public void parseProducts(List<Product> products) {
        this.products = products;

        for (Product product : products) {

            pictures.add(new ArrayList<Bitmap>());

            lastProductId = product.getId();
        }
        view.setProductsListOnMain(products);
        view.updateProducts();
        view.showMainScreen();
    }

}