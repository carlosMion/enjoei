package br.com.mion.enjoeichallenge.SalesList;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import br.com.mion.enjoeichallenge.Retrofit.ListProducts.POJOs.Product; /**
 * Created by Carlos Mion on 27/06/2018.
 * E-mails :
 * mioncarlos23@gmail.com
 * carlosmion23@hotmail.com
 */
public interface MVPSalesList {

    interface View {

        void showErrorScreen();

        void showMainScreen();

        void showLoadingScreen();

        void updateProducts();

        void setProductsListOnMain(List<Product> products);
    }

    interface Presenter {

        void setView(MVPSalesList.View view);

        void getItemsList();

        void showErrorScreen();

        void parseProducts(List<Product> products);
    }

    interface Model {

        void setPresenter(Presenter presenter);

        void getItemsList();
    }

}
