package br.com.mion.enjoeichallenge.Util;

import android.widget.TextView;

/**
 * Created by Carlos Mion on 01/07/2018.
 * E-mails :
 * mioncarlos23@gmail.com
 * carlosmion23@hotmail.com
 */
public class TextOperations {

    public static String getTText(TextView textView) {
        return textView.getText().toString();
    }

    public static String getTText(Float price) {
        return String.valueOf(price);
    }

    public static String getTText(Integer id) {
        return String.valueOf(id);
    }
}
