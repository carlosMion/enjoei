package br.com.mion.enjoeichallenge.Util;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.view.View;
import android.widget.FrameLayout;

import br.com.mion.enjoeichallenge.R;

/**
 * Criado por Carlos Mion em 30/05/2018.
 * Emails:
 * carlosmion23@hotmail.com
 * mion.carlos23@gmail.com
 */

public class UINavigation {

    public static void callFragment(Fragment fragment, String tag, Activity context) {
        FrameLayout fragment_placeholder = context.findViewById(R.id.fragment_placeholder);
        FragmentTransaction ft = context.getFragmentManager().beginTransaction();
        ft.addToBackStack(tag);
        fragment_placeholder.setVisibility(View.VISIBLE);
        ft.replace(R.id.fragment_placeholder, fragment, tag).commit();
    }

}