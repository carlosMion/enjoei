package br.com.mion.enjoeichallenge.Retrofit.ListProducts.POJOs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Carlos Mion on 27/06/2018.
 * E-mails :
 * mioncarlos23@gmail.com
 * carlosmion23@hotmail.com
 */
public class Photo {

    @SerializedName("public_id")
    @Expose
    private String publicId;
    @SerializedName("crop")
    @Expose
    private String crop;
    @SerializedName("gravity")
    @Expose
    private String gravity;

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public String getCrop() {
        return crop;
    }

    public void setCrop(String crop) {
        this.crop = crop;
    }

    public String getGravity() {
        return gravity;
    }

    public void setGravity(String gravity) {
        this.gravity = gravity;
    }

}