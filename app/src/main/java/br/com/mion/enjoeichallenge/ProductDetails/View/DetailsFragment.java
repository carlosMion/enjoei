package br.com.mion.enjoeichallenge.ProductDetails.View;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.BaseSliderView;
import com.glide.slider.library.SliderTypes.TextSliderView;
import com.glide.slider.library.Tricks.ViewPagerEx;

import java.util.ArrayList;

import br.com.mion.enjoeichallenge.ProductDetails.MVPDetails;
import br.com.mion.enjoeichallenge.ProductDetails.Presenter.DetailsPresenter;
import br.com.mion.enjoeichallenge.R;
import br.com.mion.enjoeichallenge.Retrofit.ListProducts.POJOs.Photo;
import br.com.mion.enjoeichallenge.Retrofit.ListProducts.POJOs.Product;
import br.com.mion.enjoeichallenge.Util.Constants;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static br.com.mion.enjoeichallenge.Util.TextOperations.getTText;

/**
 * Created by Carlos Mion on 01/07/2018.
 * E-mails :
 * mioncarlos23@gmail.com
 * carlosmion23@hotmail.com
 */
public class DetailsFragment extends Fragment implements BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener, MVPDetails.View {

    @BindView(R.id.slider)
    SliderLayout sliderLayout;
    @BindView(R.id.tv_payment_description)
    TextView tvPayment_desc;
    @BindView(R.id.tv_actual_price)
    TextView tvActualPrice;
    @BindView(R.id.tv_previous_price)
    TextView tvOriginalPrice;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_description)
    TextView tvDescription;
    @BindView(R.id.tv_commentss_count)
    TextView tvCommentsCount;

    private Product product;
    private MVPDetails.Presenter presenter;
    private ArrayList<String> listUrl = new ArrayList<>();
    private RequestOptions requestOptions;

    @OnClick(R.id.btn_back)
    void OnClickListenerBtnBack() {
        getActivity().onBackPressed();
    }

    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        android.view.View view = inflater.inflate(R.layout.fragment_details, parent, false);

        ButterKnife.bind(this, view);

        initPresenter(this);

        initSlider();

        initInfo();

        return view;
    }

    private void initInfo() {

        setActualPrice();

        setOriginalPrice();

        setPaymentDescription();

        setTitle();

        setDescription();

        setCommentsCount();


    }

    private void setCommentsCount() {

        if (product.getPublishedCommentsCount() > 0) {
            tvCommentsCount.setVisibility(View.VISIBLE);
            tvCommentsCount.setText(getTText(product.getPublishedCommentsCount()));
        }

    }

    private void setDescription() {
        tvDescription.setText(product.getContent());
    }

    private void setTitle() {

        tvTitle.setText(product.getTitle());

    }

    private void setOriginalPrice() {

        String originalPrice = "";

        if (product.getDiscountPercentage() > 0)
            originalPrice = "R$ " + getTText(product.getOriginalPrice());

        tvOriginalPrice.setText(originalPrice);
        tvOriginalPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);

    }

    private void setActualPrice() {

        String actualPrice = " " + getTText(product.getPrice());

        tvActualPrice.setText(actualPrice);

    }

    private void setPaymentDescription() {

        String payment_descreption = "";

        if (product.getDiscountPercentage() > 0)
            payment_descreption = product.getDiscountPercentage() + "% " + "off ";
        if (product.getMaximumInstallment() > 0)
            payment_descreption += "em até " + product.getMaximumInstallment() + " x";

        tvPayment_desc.setText(payment_descreption);

    }

    private void initSlider() {

        for (int i = 0; i < listUrl.size(); i++) {
            TextSliderView sliderView = new TextSliderView(getActivity());
            // if you want show image only / without description text use DefaultSliderView instead

            // initialize SliderLayout
            sliderView
                    .image(listUrl.get(i))
                    .setRequestOption(requestOptions)
                    .setBackgroundColor(Color.WHITE)
                    .setProgressBarVisible(true)
                    .setOnSliderClickListener(this);

            //add your extra information
            sliderLayout.addSlider(sliderView);
        }

        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Default);

    }

    private void initPresenter(MVPDetails.View view) {
        this.presenter = new DetailsPresenter();
        presenter.setView(view);
    }

    @SuppressLint("CheckResult")
    public void setProduct(Product product) {
        this.product = product;


        for (Photo photo : product.getPhotos()) {
            listUrl.add(Constants.URL_PHOTO + photo.getPublicId());
//            Glide.with(getActivity()).asBitmap().load(Constants.URL_PHOTO + photo.getPublicId()).into(thumbnail);
        }

        requestOptions = new RequestOptions();
        requestOptions.centerCrop();

    }

    @Override
    public void onSliderClick(BaseSliderView baseSliderView) {

    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }
}
