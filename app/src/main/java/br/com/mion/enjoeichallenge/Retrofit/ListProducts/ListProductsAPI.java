package br.com.mion.enjoeichallenge.Retrofit.ListProducts;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by Carlos Mion on 27/06/2018.
 * E-mails :
 * mioncarlos23@gmail.com
 * carlosmion23@hotmail.com
 */
public interface ListProductsAPI {

    @GET
    Call<ListProductsApiResult> getProductsList(@Url String url);

    @GET
    Call<ResponseBody> getProductPhoto(@Url String url);

}
