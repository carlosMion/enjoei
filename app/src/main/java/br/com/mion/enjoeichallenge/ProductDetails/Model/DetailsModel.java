package br.com.mion.enjoeichallenge.ProductDetails.Model;

import br.com.mion.enjoeichallenge.ProductDetails.MVPDetails;

/**
 * Created by Carlos Mion on 01/07/2018.
 * E-mails :
 * mioncarlos23@gmail.com
 * carlosmion23@hotmail.com
 */
public class DetailsModel implements MVPDetails.Model {

    /**
     * Este Model foi deixado aqui de propósito, para demonstrar que caso seja necessário, basta utilizá-lo
     */

    private MVPDetails.Presenter presenter;

    @Override
    public void setPresenter(MVPDetails.Presenter presenter) {
        this.presenter = presenter;
    }
}
