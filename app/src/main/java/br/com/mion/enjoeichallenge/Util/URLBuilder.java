package br.com.mion.enjoeichallenge.Util;

/**
 * Created by Carlos Mion on 29/06/2018.
 * E-mails :
 * mioncarlos23@gmail.com
 * carlosmion23@hotmail.com
 */
public class URLBuilder {

    public static String getGroupImageUrl(String group_id) {
        return Constants.URL_PHOTO + group_id + ".jpg";
    }

}